Source: pprintpp
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>,
Build-Depends: debhelper (>= 9),
               debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-setuptools,
Standards-Version: 4.6.0.1
Homepage: https://github.com/wolever/pprintpp
Vcs-Git: https://salsa.debian.org/python-team/packages/pprintpp.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/pprintpp

Package: python3-pprintpp
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
Recommends: ${python3:Recommends},
Suggests: ${python3:Suggests},
Description: drop-in replacement for pprint that's actually pretty
 Unlike pprint, pprintpp strives to emit a readable, largely PEP8-compliant,
 representation of its input.
 .
 It also has explicit support for: the collections module (defaultdict and
 Counter) and numpy arrays.
 .
 Unicode characters, when possible, will be printed un-escaped. This is done by
 checking both the output stream's encoding (defaulting to utf-8) and the
 character's Unicode category. An effort is made to print only characters which
 will be visually unambiguous: letters and numbers will be printed un-escaped,
 spaces, combining characters, and control characters will be escaped.
 .
 Subclassess of built-in collection types which don't define a new __repr__ will
 have their class name explicitly added to their repr (while custom __repr__
 methods will be respected).
